import React from 'react';

import {InputWithText} from '../components/InputWithText';


class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "aditya",
            password: "jobin"
        };
    }
    handleChangeUsername(e) {
        // console.log(e.target.value);
        // console.log(this);
        this.setState({username: e.target.value})
    }
    handleChangePassword(e) {
        // console.log(e.target.value);
        // console.log(this);
        this.setState({password: e.target.value})
    }

    handleButtonClick() {
        console.log(this.state);

    }
    render() {
        return (
            <div>
                <InputWithText type="text"
                               text={this.state.username}
                               onChangeHandler={this.handleChangeUsername.bind(this)}/>

                <InputWithText type="password"
                               text={this.state.password}
                                onChangeHandler={this.handleChangePassword.bind(this)}/>
                <button onClick={this.handleButtonClick.bind(this)}>Log in</button>
            </div>
        )
    }
}



export default Home;