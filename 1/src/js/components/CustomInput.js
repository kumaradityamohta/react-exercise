import React from 'react';



class CustomInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: "aditya"
        };
    }
    handleChange(e) {
        console.log(e.target.value);

        // console.log(this);
        this.setState({text: e.target.value})
    }
    render() {

        return (
            <div>
                <p>{this.state.text}</p>
                <input type="text" onChange={this.handleChange.bind(this)}
                       placeholder={this.state.text}/>
            </div>
        )
    }
}



// const CustomInput = (props) => (
//     <div>
//         <p>text</p>
//         <input type="text"/>
//     </div>
// );

export default CustomInput;