import React from 'react';


export const InputWithText = (props) => (
    <div>
        <p>{props.text}</p>
        <input type={props.type} value={props.text} onChange={props.onChangeHandler}/>
    </div>
);

