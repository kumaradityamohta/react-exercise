import React from 'react';



const Header = ({title="some time", text = "som etext"}) => (
    <header className="header">
        <p>{title}</p>
        <p>{text}</p>
    </header>
);

export default Header;