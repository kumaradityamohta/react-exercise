var webpack = require('webpack');
var path = require('path');

module.exports = {
    devtool: 'cheap-module-source-map',   // this gives line numbers in webpack logs
    devServer: {
        inline: true,
        contentBase: './public',
        port: 3000,
        historyApiFallback: true,   // 404 pages fall back on index.html
        disableHostCheck: true // to enable all hosts
    },
    entry: './src/js/index.js',
    output: {   // this is only for production, so we don't need it for now.
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    resolve: {
        modules: ['node_modules', 'src'],
        extensions: ['.js']
    },
    module: {   // this is where we define our loaders.
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader?presets[]=react,presets[]=env,presets[]=stage-2'],
                exclude: /node_modules/
            },
        ]  // to transform JSX to JS
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),   // this is to use react-hot package for live reloading.
        new webpack.NoEmitOnErrorsPlugin()      // webpack won't compile if there are errors if we use this plugin.
    ]
};